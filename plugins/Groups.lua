local function chat_list(msg)
    local data = load_data(_config.moderation.data)
        local groups = 'groups'
        if not data[tostring(groups)] then
                return 'گروه یافت نشد!'
        end
        local message = 'کاربر گرامي براي وارد شدن در گروهاي ربات تله گولد به صورت زير عمل کنيد:\n\nورود (شماره گروه)\nو براي دريافت لينک بصورت زير:\nارسال لينک (شماره گروه)\nمثال:\nارسال لينک 628266629'
        for k,v in pairs(data[tostring(groups)]) do
                local settings = data[tostring(v)]['settings']
                for m,n in pairsByKeys(settings) do
                        if m == 'set_name' then
                                name = n
                        end
                end

                message = message .. '- '.. name .. ' (شماره گروه ' .. v .. ')\n………………………………………\n '
        end
        local file = io.open("./groups/lists/listed_groups.txt", "w")
        file:write(message)
        file:flush()
        file:close()
        return message
end
local function run(msg, matches)
  if msg.to.type ~= 'chat' or is_sudo(msg) and is_realm(msg) then
  local data = load_data(_config.moderation.data)
  if is_sudo(msg) then
    if matches[1] == 'لینک' and data[tostring(matches[2])] then
        if is_banned(msg.from.id, matches[2]) then
     return 'شما از این گروه بن شدید'
  end
      if is_gbanned(msg.from.id) then
            return 'شما از همه گروها بن شدید'
      end
      if data[tostring(matches[2])]['settings']['lock_member'] == 'yes' and not is_owner2(msg.from.id, matches[2]) then
        return 'این گروه عمومی است.'
      end
          local chat_id = "chat#id"..matches[2]
          local user_id = "user#id"..msg.from.id
      chat_add_user(chat_id, user_id, ok_cb, false)   
   local group_link = data[tostring(matches[2])]['settings']['set_link']
   local group_name = data[tostring(matches[2])]['settings']['set_name']
   return "TeleGoldⓒ вот\n…………………………\n"..group_link.."\n\nنام گروه ("..group_name..")\nقدرت گرفته توسط تله گولد 💪"
   
    elseif matches[1] == 'ارسال لینک' and not data[tostring(matches[2])] then

          return "گروه یافت نشد!"
         end
    end
  else
   return "فقط سازندگان!"
  end

     if matches[1] == 'گروها' then
      if is_sudo(msg) and msg.to.type == 'chat' then
         return chat_list(msg)
       elseif msg.to.type ~= 'chat' then
         return chat_list(msg)
      end
 end
 end
return {
    description = "See link of a group and groups list",
    usage = "!link ID && !groups",
patterns = {"^(ارسال لینک) (.*)$","^(گروها)$"},
run = run
}
