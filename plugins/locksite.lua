local function run(msg)

local data = load_data(_config.moderation.data)

if data[tostring(msg.to.id)]['settings']['lock_ads'] == 'yes' then


if msg.to.type == 'channel' and not is_momod(msg) then
delete_msg(msg.id,ok_cb,false)

return 
end
end
end

return {patterns = {
".[cC][oO][mM]",
".[iI][Rr]",
".[iI][nN]",
".[tT][Kk]",
".[oO][rR][gG]",
".[mM][Ee]",
".[iI][oO]",
".[nN][eE][tT]",
".[uU][dD][uU]",
".[Gg][oO][vV]",
".[bB][iI][zZ]",
".[iI][nN][tT]",
".[iI][nN][fF]",
".[wW][eE][bB]",
".[nN][Ee][mM][eE]",
".[aA][cC]",
".[wW][sS]",
".[uU][sS]",
".[tT][vV]",
".[aA][eE][rR][oO]",
".[mM][iI][lL]",
".[cC][aA][Tt]",
".[cC][oO][oO][pP]",
".[jJ][oO][pP][sS]",
".[mM][oO][bB][iI]",
".[pP][rR][oO]",
".[rR][rR][Aa][vV][eE][lL]",
}, run = run}
